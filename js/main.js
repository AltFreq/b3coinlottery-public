/**
 * ===================================================================
 * main js
 *
 * -------------------------------------------------------------------
 */

if (!String.prototype.startsWith) {
    (function() {
        'use strict'; // needed to support `apply`/`call` with `undefined`/`null`
        var defineProperty = (function() {
            // IE 8 only supports `Object.defineProperty` on DOM elements
            try {
                var object = {};
                var $defineProperty = Object.defineProperty;
                var result = $defineProperty(object, object, object) && $defineProperty;
            } catch(error) {}
            return result;
        }());
        var toString = {}.toString;
        var startsWith = function(search) {
            if (this == null) {
                throw TypeError();
            }
            var string = String(this);
            if (search && toString.call(search) == '[object RegExp]') {
                throw TypeError();
            }
            var stringLength = string.length;
            var searchString = String(search);
            var searchLength = searchString.length;
            var position = arguments.length > 1 ? arguments[1] : undefined;
            // `ToInteger`
            var pos = position ? Number(position) : 0;
            if (pos != pos) { // better `isNaN`
                pos = 0;
            }
            var start = Math.min(Math.max(pos, 0), stringLength);
            // Avoid the `indexOf` call if no match is possible
            if (searchLength + start > stringLength) {
                return false;
            }
            var index = -1;
            while (++index < searchLength) {
                if (string.charCodeAt(start + index) != searchString.charCodeAt(index)) {
                    return false;
                }
            }
            return true;
        };
        if (defineProperty) {
            defineProperty(String.prototype, 'startsWith', {
                'value': startsWith,
                'configurable': true,
                'writable': true
            });
        } else {
            String.prototype.startsWith = startsWith;
        }
    }());
}

(function($) {

    "use strict";

    /*---------------------------------------------------- */
    /* Preloader
	------------------------------------------------------ */
    $(window).load(function() {

        // will first fade out the loading animation
        $("#loader").fadeOut("slow", function() {

            // will fade out the whole DIV that covers the website.
            $("#preloader").delay(300).fadeOut("slow");

        });

    });


    /*----------------------------------------------------*/
    /*	Sticky Navigation
	------------------------------------------------------*/
    $(window).on('scroll', function() {

        var y = $(window).scrollTop(),
            topBar = $('header');

        if (y > 1) {
            topBar.addClass('sticky');
        } else {
            topBar.removeClass('sticky');
        }

    });


    /*-----------------------------------------------------*/
    /* Mobile Menu
	------------------------------------------------------ */
    var toggleButton = $('.menu-toggle'),
        nav = $('.main-navigation');

    toggleButton.on('click', function(event) {
        event.preventDefault();

        toggleButton.toggleClass('is-clicked');
        nav.slideToggle();
    });

    if (toggleButton.is(':visible')) {
        nav.addClass('mobile');
    }

    $(window).resize(function() {
        if (toggleButton.is(':visible')) {
            nav.addClass('mobile');
        } else {
            nav.removeClass('mobile');
        }
    });

    $('#main-nav-wrap li a').on("click", function() {

        if (nav.hasClass('mobile')) {
            toggleButton.toggleClass('is-clicked');
            nav.fadeOut();
        }
    });


    /*----------------------------------------------------*/
    /* Highlight the current section in the navigation bar
	------------------------------------------------------*/
    var sections = $("section"),
        navigation_links = $("#main-nav-wrap li a");

    sections.waypoint({

        handler: function(direction) {

            var active_section;

            active_section = $('section#' + this.element.id);

            if (direction === "up") active_section = active_section.prev();

            var active_link = $('#main-nav-wrap a[href="#' + active_section.attr("id") + '"]');

            navigation_links.parent().removeClass("current");
            active_link.parent().addClass("current");

        },

        offset: '25%'

    });



    /*----------------------------------------------------*/
    /* Flexslider
	/*----------------------------------------------------*/
    $(window).load(function() {

        $('#testimonial-slider').flexslider({
            namespace: "flex-",
            controlsContainer: "",
            animation: 'slide',
            controlNav: true,
            directionNav: true,
            smoothHeight: true,
            slideshowSpeed: 7000,
            animationSpeed: 600,
            randomize: false,
            touch: true,
        });

    });


    /*----------------------------------------------------*/
    /* Smooth Scrolling
	------------------------------------------------------*/
    $('.smoothscroll').on('click', function(e) {

        if($("#intro-table").length){
            e.preventDefault();

            var target = this.hash,
                $target = $(target);

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 800, 'swing', function () {
                window.location.hash = target;
            });
        }
    });


    /*----------------------------------------------------*/
    /*  Placeholder Plugin Settings
	------------------------------------------------------*/

    $('input, textarea, select').placeholder()





    /*----------------------------------------------------- */
    /* Back to top
   ------------------------------------------------------- */
    var pxShow = 300; // height on which the button will show
    var fadeInTime = 400; // how slow/fast you want the button to show
    var fadeOutTime = 400; // how slow/fast you want the button to hide
    var scrollSpeed = 300; // how slow/fast you want the button to scroll to top. can be a value, 'slow', 'normal' or 'fast'

    // Show or hide the sticky footer button
    jQuery(window).scroll(function() {

        if (!($("#header-search").hasClass('is-visible'))) {

            if (jQuery(window).scrollTop() >= pxShow) {
                jQuery("#go-top").fadeIn(fadeInTime);
            } else {
                jQuery("#go-top").fadeOut(fadeOutTime);
            }

        }

    });

})(jQuery);


var b3_wallet = null;
var generated_wallet = null;
var generated_link = null;
var generated_node = null;
var isreferred = isReffered()
var referred_by = null;
var auto_play = null;

function getWidth(){
    return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
}

function isLoggedIn() {
    wallet = localStorage.getItem("B3_Wallet1");
    if (wallet != null) {
        b3_wallet = wallet;
        generated_wallet = localStorage.getItem('Generated_Wallet1');
        if (generated_wallet == null) {
            return false;
        }
        generated_link = localStorage.getItem('Generated_Link1');
        generated_node = localStorage.getItem('Generated_Node1');
        auto_play = localStorage.getItem("Auto_Play1")=="true";
        return true;
    }
    return false;
}

function isReffered() {
    ref = localStorage.getItem("referred_by");
    if (ref != null) {
        referred_by = ref;
        return true;
    }
    return false;
}


function check_wallet_address(address) {
    if (address.startsWith("S")) {
        if (address.trim().length == 34) {
            return true;
        }
    }
    return false;
}

function check_wallet_warning(string) {
    $('#input-warning').show();
    $('#input-warning').html("<p>" + string + "</p>");
}

function check_wallet_warning_hide() {
    $('#input-warning').hide();
}

function store_b3_wallet(b3_wallet) {
    localStorage.setItem("B3_Wallet1", b3_wallet);
}

function store_generated(generated_wallet) {
    localStorage.setItem('Generated_Wallet1', generated_wallet);
}

function store_auto_play(auto_play){
    localStorage.setItem('Auto_Play1', auto_play);
}

function check_for_generated() {
    if (generated_wallet != null) {
        $('#wallet-address').val(b3_wallet);
        $('#wallet-address').blur();
        $('#generated-wallet').show();
        $('#gen-wal').val(generated_wallet);
        $('#b3-wal').text(b3_wallet);
        $('#wallet-address').hide();
    }

}

function store_generated_link(link) {
    localStorage.setItem('Generated_Link1', link);
}

function store_generated_node(node){
    localStorage.setItem("Generated_Node1", node);
}

function getRefferalUsed() {
    if (referred_by == null) {
        return " ";
    }
    return ref;
}

function add_wallet_listeners() {
    $('#wallet-address').keyup(function() {
        input = $('#wallet-address').val();
        check = check_wallet_address(input);
        if (check) {
            store_b3_wallet(input);
            check_wallet_warning_hide();
        } else {
            check_wallet_warning("Incorrect B3 Wallet Address");
        }
    });

    $('#wallet-address').bind('paste', function() {
        setTimeout(function() {
            input = $('#wallet-address').val();
            check = check_wallet_address(input);
            if (check) {
                store_b3_wallet(input);
                check_wallet_warning_hide();
            } else {
                check_wallet_warning("Incorrect B3 Wallet Address");
            }
        }, 200);
    });

}


$('#table-search-input').keyup(function() {
    input = $('#table-search-input').val();
    $('#current-players').find("tr").hide();
    $('#current-players').find("tr:contains(" + input + ")").show();

})

$('#table-search-input').bind('paste', function() {
    setTimeout(function() {
        input = $('#table-search-input').val();
        $('#current-players').find("tr").hide();
        $('#current-players').find("tr:contains(" + input + ")").show();
    }, 100);
});


if ($('#wallet-address').length) {
    check_for_generated();
}


function includeHTML(cb) {
    var z, i, elmnt, file, xhttp;
    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
        elmnt = z[i];
        file = elmnt.getAttribute("w3-include-html");
        if (file) {
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4) {
                    if (this.status == 200) {
                        elmnt.innerHTML = this.responseText;
                    }
                    if (this.status == 404) {
                        elmnt.innerHTML = "Page not found.";
                    }
                    elmnt.removeAttribute("w3-include-html");
                    includeHTML(cb);
                }
            }
            xhttp.open("GET", file, true);
            xhttp.send();
            return;
        }
    }
    if (cb) cb();
}


function get_Random_Timer() {
    return Math.floor((Math.random() * 120000) + 5000)
}


if ($('#referral-code').length) {
    var ref = localStorage.getItem('referred_by');
    if (ref == null) {
        localStorage.setItem('referred_by', $('#referral-code').text());
    }
}


if ($('#player_tickets').length) {
    var wallet = localStorage.getItem('B3_Wallet1');
    if (wallet != null) {
        $.ajax({
            type: "GET",
            url: "main.php",
            data: {
                func: "getPlayerTickets",
                b3wallet: wallet
            },
            success: function(text) {
                if (text) {
                    $('#player_tickets').html('Your Confirmed Tickets: <span id="confirmed_tickets">' + text+'</span>');
                } else {
                    $('#player_tickets').html('Your Confirmed Tickets: <span id="confirmed_tickets">' + 0 + '</span>');
                }
                $('#player_tickets').show();
            }
        });
    } else {
        $('#player_tickets').hide();
    }

    var blocknumber = 0;
    setInterval(function(){
        string = $('#timer').text();
        if(string.startsWith("Wait")){

        }else{
            $.ajax({
                type: "GET",
                url: "https://chainz.cryptoid.info/b3/api.dws?q=getblockcount",
                success: function(text) {
                    if(blocknumner=0){
                        blocknumber = text;
                    }else if(blocknumber!=text){
                        //                        $.ajax({
                        //                            type: "GET",
                        //                            url: "main.php",
                        //                            data: {
                        //                                func: "updateActivePage",
                        //                                b3wallet: wallet
                        //                            },
                        //                            success: function(data) {
                        ////                                json = jQuery.parseJSON(data); //json.table
                        ////                                console.log(json);
                        //                            }
                        //                        });
                        blocknumber = text;
                    }
                }
            });
        }
    },120000);
}

if ($('#timer').length) {
    string = $('#timer').text();
    if (!string.startsWith("Wait")) {
        split = string.split(':');
        hour = parseInt(split[0]);
        minut = parseInt(split[1]);
        sec = parseInt(split[2]);
        setInterval(function() {
            if (sec === 0 && minut === 0 && hour === 0) {
                setTimeout(function() {
                    location.reload();
                }, get_Random_Timer());
            } else if (sec - 1 < 0) {
                if (minut - 1 < 0) {
                    if (hour - 1 < 0) {
                        setTimeout(function() {
                            location.reload();
                        }, get_Random_Timer());
                    } else {
                        hour -= 1;
                        min = 59
                    }
                } else {
                    minut -= 1;
                    sec = 59;
                }
            } else {
                sec -= 1;
            }
            time = ""
            if (hour < 10) {
                time += "0" + hour + ":";
            } else {
                time += hour + ":";
            }
            if (minut < 10) {
                time += "0" + minut + ":";
            } else {
                time += minut + ":";
            }
            if (sec < 10) {
                time += "0" + sec;
            } else {
                time += sec;
            }
            $('#timer').text(time);
        }, 1000);
    }
}

if ($('#previous_links').length) {
    $('#previous_links a').click(function(e) {
        $('#previous_rounds').addClass('loading');
        id = e.target.text;
        $.ajax({
            type: "GET",
            url: "main.php",
            data: {
                func: "getPreviousRound",
                roundID: id
            },
            success: function(res) {
                json = jQuery.parseJSON(res);
                $('#previous_round_body').html(json.table);
                $('#previous_round_number').text(id);
                $('#previous_round_tickets').text(json.tickets);
                $('#previous_rounds').removeClass('loading');
            }
        });

        return false;
    });
}

if ($('#previous_giveaway_links').length) {
    $('#previous_giveaway_links a').click(function(e) {
        $('#previous_loader').addClass('loading');
        id = e.target.text;
        $.ajax({
            type: "GET",
            url: "main.php",
            data: {
                func: "getPreviousGiveaway",
                roundID: id
            },
            success: function(res) {
                json = jQuery.parseJSON(res);
                $('#previous_giveaway_body').html(json.table);
                $('#previous_date').text(json.date);
                $('#previous_total_tickets').text(json.total_tickets);
                $('#previous_prize_pool').text(json.amount + " kB3");
                $('#previous_rounds').removeClass('loading');
            }
        });

        return false;
    });
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

if ($('#total_paid').length) {
    $.get("https://api.coinmarketcap.com/v1/ticker/b3coin/", function(data, status) {
        $('#btc_paid').text((Math.round($('#total_paid').text() * data[0]['price_btc'] * 1000 * 10000) / 100000).toLocaleString() + " BTC");
        $('#usd_paid').text("$" + (Math.round($('#total_paid').text() * data[0]['price_usd'] * 100 * 100) / 100).toLocaleString());
        $('#total_paid').text(($('#total_paid').text() * 1).toLocaleString())
    });
}


var modal = (function() {
    var
    method = {},
        $overlay,
        $modal,
        $content,
        $close;

    // Center the modal in the viewport
    method.center = function() {
        var top, left;

        //		top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
        top = "10%";
        left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;
        max = "89%";

        $modal.css({
            top: top,
            left: left,
            "max-height": max
        });
    };

    // Open the modal
    method.open = function(settings) {
        $content.empty().append(settings.content);
        $content.css({
            width: '100%',
            height: '100%'
        });
        if(screen.width<650){
            $modal.css({
                width: settings.width || '100%',
                height: settings.height || '400px'
            });
        }else{
            $modal.css({
                width: settings.width || '66%',
                height: settings.height || '400px'
            });
        }

        method.center();
        $(window).bind('resize.modal', method.center);
        $modal.show();
        $overlay.show();
    };

    // Close the modal
    method.close = function() {
        $modal.hide();
        $overlay.hide();
        $content.empty();
        $(window).unbind('resize.modal');
    };

    // Generate the HTML and add it to the document
    $overlay = $('<div id="overlay"></div>');
    $modal = $('<div id="modal"></div>');
    $content = $('<div id="content"></div>');
    $close = $('<a id="close" href="#">close</a>');

    $modal.hide();
    $overlay.hide();
    $modal.append($content, $close);

    $(document).ready(function() {
        $('body').append($overlay, $modal);
    });

    $close.click(function(e) {
        e.preventDefault();
        method.close();
    });

    return method;
}());

var adblock = false;

var adBlockDetected = function() {
    adblock = true;
}
var adBlockUndetected = function() {
    adblock = false;
}

if(typeof FuckAdBlock === 'undefined') {
    $(document).ready(adBlockDetected);
} else {
    var FabPluginGoogleAdsense = function() {
        FuckAdBlock.getPluginClass().apply(this, arguments);
        this.options.set({
            windowVar: 'adsbygoogle',
        });

        var data = {};

        this.start = function() {
            var self = this;
            var windowVar = this.options.get('windowVar');
            data.loopTimeout = setTimeout(function() {
                if(window[windowVar].loaded === true) {
                    self.callUndetected();
                } else {
                    self.callDetected();
                }
            }, 1);
            return this;
        };
        this.stop = function() {
            clearInterval(data.loopTimeout);
            clearInterval(data.loopInterval);
            return this;
        };
    };
    FabPluginGoogleAdsense.pluginName = 'google-adsense';
    FabPluginGoogleAdsense.versionMin = [4, 0, 0];

    var myFuckAdBlock = new FuckAdBlock;
    myFuckAdBlock.registerPlugin(FabPluginGoogleAdsense);
    myFuckAdBlock.on(true, adBlockDetected).on(false, adBlockUndetected);
    $(document).ready(function() {
        myFuckAdBlock.check(['google-adsense']);
    });
}

// Wait until the DOM has loaded before querying the document
$(document).ready(function() {
    add_login_modal();
    add_node_status_modal();
    add_deposit_modal();
    add_auto_play_modal();
    add_generate_modal();
    setTimeout(function(){
        if($("#captcha").height() === 0){
            $('#captcha').html('<center><div id="html_element"></div></center>');
            grecaptcha.render('html_element', {
                'sitekey' : '6LfOXEwUAAAAACGwAIYA_qZAMbB5koZdEkMHvjdR'
            });
        }
    }, 1000);


    if($("#captcha").length){
        $("#submit_area").click(function(){
            if($(".g-recaptcha").length){
                var data = null;
                data = {
                    func:"captcha",
                    g_recaptcha_response: grecaptcha.getResponse(),
                    b3_wallet: b3_wallet
                };
            }else{
                data = {
                    func:"captcha",
                    adcopy_challenge: $('#adcopy_challenge').val(),
                    adcopy_response: $("#adcopy_response").val(),
                    b3_wallet: b3_wallet
                }
            }
            $.ajax({
                type: "POST",
                url: "main.php",
                data: data,
                success: function(text)
                {
                    console.log(text);
                    result = text.substr(0, text.indexOf(' '));
                    if(adblock){
                        $("#captcha_error").html("<center>Captcha Error - Adblock enabled</center>" );
                        $("#captcha_error").css("color", "red");
                        $("#captcha_next").html("<center>Refreshing page in 10 seconds...</center>");
                        $("#captcha_next").css("color", "red");
                        setTimeout(function(){
                            location.reload(false)
                        }, 11000);
                        $("#submit_area").hide();
                    }else if(result=="Success"){
                        $("#captcha_error").html("<center>Captcha success</center>");
                        $("#captcha_error").css("color", "green");

                        $("#captcha_next").html("<center>"+text.substr(text.indexOf(' ')+1)+"</center>");
                        $("#captcha_next").css("color", "green");
                    }else{
                        $("#captcha_error").html("<center>Error - " + text.substr(text.indexOf(' ')+1) + "</center>" );
                        $("#captcha_error").css("color", "red");

                        $("#captcha_next").html("<center>Refreshing page in 5 seconds...</center>");
                        $("#captcha_next").css("color", "red");
                        setTimeout(function(){
                            location.reload(false)
                        }, 6000);
                        $("#submit_area").hide();
                    }
                }
            });
        });
    }
});

function add_login_modal() {
    $('#login_button ').click(function(e) {
        modal.open({
            content: '<div id="top_modal"><center><h1>Login</h1></center></div>\
<div id="input-holder">\
<center><h2>Welcome To B3 Lottery!</h2></center>\
<label>Wallet Address:</label>\
<input id="wallet-address" placeholder="Enter your B3 wallet address">\
<div id="input-warning">\
<p> This is a warning message </p>\
</div>\
<div id="terms">\
Please ensure your have read the <a href="tos.html">TOS</a>, by logging in you accept these terms\
</div>\
<center><a class="button stroke" href="#" title="" id="login_confirm">Login</a></center>\
</div>'
        });
        add_wallet_listeners();
        add_login_listener();
        e.preventDefault();
    });
}

function add_generate_modal() {
    if($('#generate_button').length){
        $('#generate_button ').click(function(e) {
            modal.open({
                content: '<div id="top_modal"><center><h1>Lottery Deposit Address</h1></center></div>\
<center><h2><span class="warning">Warning: </span>Please read the terms below carefully!</h2></center>\
<div id="deposit_container">\
<div id="inner_deposit_container">\
<ul>\
<li>All deposits are final.</li>\
<li>There will be no refunds for any accepted amounts</li>\
<li>Only deposits in multiples of 10kB3 will be accepted. Eg. 30kB3 or 14000kB3</li>\
<li>Deposits will remain pending until 1 confirmation.</li>\
<li>Any amounts deposited less than 10kb3 will be refunded minus a small fee.</li>\
</ul>\
</div>\
</div>\
<div id="terms">\
Please ensure your have read the <a href="tos.html">TOS</a>, by depositing to this generated address you accept and agree to these terms\
</div>\
<center><a class="button stroke" href="#" title="" id="deposit_confirm">I Agree</a>\
<div id="generated-wallet" style="display:none; width:80%;">\
<p style="color:#fff"> Deposit your kB3 to this address to enter the lottery:<br>\
<b><u><input id="gen-wal" placeholder="" readonly style="display: inline-block; background-color: rgb(235,235,228)"></u></p>\
</div></center>'
            });
            $('#modal').css("height","480px");

            $('#deposit_confirm').click(function(){
                if(b3_wallet==null){
                    alert("Please login first (Only requires a B3 address)");
                    return;
                }
                $('#deposit_confirm').hide();
                $('#generated-wallet').show();
                $('#gen-wal').val("Generating address...");
                if(generated_wallet!=null){
                    $('#gen-wal').val(generated_wallet);
                }else{
                    $.ajax({
                        type: "GET",
                        url: "main.php",
                        data: {
                            func: "generate",
                            wallet: b3_wallet,
                            referral_used: referred_by
                        },
                        success: function(text)
                        {
                            generated_wallet = text;
                            localStorage.setItem("B3_Wallet1", text);
                            $('#gen-wal').val(text);
                        }
                    });
                }
            });
        });
    }
}

function add_auto_play_modal(){
    if($('#myonoffswitch').length){
        $('#myonoffswitch').click(function(e) {
            if(b3_wallet==null){
                alert("Please login first");
                e.preventDefault(true);
                return;
            }
            if(auto_play){
                $.ajax({
                    type: "GET",
                    url: "main.php",
                    data: {
                        func: "auto_play",
                        wallet: b3_wallet
                    },
                    success: function(text) //enabled /disabled truefalse
                    {
                        console.log("play =" + text);
                        store_auto_play(text=='1');
                        auto_play = text=='1';
                        update_login_data();
                    }
                });
                e.preventDefault(true);
                return;
            }
            modal.open({
                content: '<div id="top_modal"><center><h1>Lottery Auto Play</h1></center></div>\
<center><h2><span class="warning">Warning: </span>Please read the terms below carefully!</h2></center>\
<div id="deposit_container">\
<div id="inner_deposit_container">\
<ul>\
<li>With Auto Play Enabled you will no longer have funds returned straight to your wallet at the end of the round</li>\
<li>Any remaining funds at the end of the round will be placed into the next round.</li>\
<li>Users funds will be used to purchase as many tickets as possible per the ticket price.</li>\
<li>Remaining funds after ticket purchase will be stored until the next round.</li>\
<li>Funds that do not meet the minimum 10kb3 will be returned to the user and Auto Play disabled</li>\
<li>If funds are in Auto Play and Auto Play is disabled, the funds will not be returned until the round has completed.</li>\
<li>Turning off Auto Play during a round will not remove your coins instantly from the current round. They will instead be returned at the conclusion of the round.</li>\
</ul>\
</div>\
</div>\
<div id="terms">\
Please ensure your have read the <a href="tos.html">TOS</a>\
</div>\
<center><a class="button stroke" href="#" title="" id="confirm_auto">I Agree</a>\
</center>'
            });
            $('#modal').css("height","520px");
            $("#confirm_auto").click(function(){
                modal.close();
                $.ajax({
                    type: "GET",
                    url: "main.php",
                    data: {
                        func: "auto_play",
                        wallet: b3_wallet
                    },
                    success: function(text) //enabled /disabled truefalse
                    {
                        console.log("play =" + text);
                        store_auto_play(text=='1');
                        auto_play = text=='1';
                        update_login_data();
                    }
                });
                return;
            });
            e.preventDefault(true);
        });
    }
}

function add_deposit_modal() {
    if($('#deposit_button').length){
        $('#deposit_button ').click(function(e) {
            modal.open({
                content: '<div id="top_modal"><center><h1>Direct Deposit</h1></center></div>\
<center><h2><span class="warning">Warning: </span>Please read the terms below carefully!</h2></center>\
<div id="deposit_container">\
<p>Before depositing to the generated address below for a direct deposit into the fundamental node pool there a few thinigs that you should first know:</p>\
<div id="inner_deposit_container">\
<ul>\
<li>All deposits are final.</li>\
<li>There will be no refunds for any accepted amounts</li>\
<li>Only deposits of more than or equal to 5,000 kB3 will be accepted, any amounts lower than 5,000kb3 will be automatically refunded minus a small fee of 1B3.</li>\
<li>100% of your deposit will be credited to your account after the pending period, where your deposit will remain in a pending state until the next fundamental node to be created is rewarded.</li>\
<li>While your deposit is pending you will only be credited 25% of that deposits rewards from the pool. Eg deposit 10,000 and your total balance will increase by 2,500 while pending.</li>\
<li>Deposits will remain pending for roughly 15 days while their node is out of circulation.</li>\
<li>Any rewards gained through direct deposit will incur a 2% pool fee</li>\
<li>25% of the fee will be rewarded to the referrer of the direct deposit owner.</li>\
</ul>\
</div>\
</div>\
<div id="terms">\
Please ensure your have read the <a href="tos.html">TOS</a>, by depositing to this generated address you accept and agree to these terms\
</div>\
<center><a class="button stroke" href="#" title="" id="deposit_confirm">I Agree</a>\
<div id="generated-wallet" style="display:none; width:80%;">\
<p style="color:#fff"> Deposit your kB3 to this address for a direct pool deposit:<br>\
<b><u><input id="gen-wal" placeholder="" readonly style="display: inline-block; background-color: rgb(235,235,228)"></u></p>\
</div></center>'
            });
            $('#modal').css("height","740px");

            $('#deposit_confirm').click(function(){
                if(b3_wallet==null){
                    alert("Please login first");
                    return;
                }
                $('#deposit_confirm').hide();
                $('#generated-wallet').show();
                $('#gen-wal').val("Generating address...");
                if(generated_node!=null){
                    $('#gen-wal').val(generated_node);
                }else{
                    $.ajax({
                        type: "GET",
                        url: "main.php",
                        data: {
                            func: "gen_node",
                            wallet: b3_wallet
                        },
                        success: function(text)
                        {
                            generated_node = text;
                            localStorage.setItem("Generated_Node1", text);
                            $('#gen-wal').val(text);
                        }
                    });
                }
            });
        });
    }
}


function add_node_status_modal() {
    if($('#view_node_status').length){
        $('#view_node_status ').click(function(e) {
            modal.open({
                content: '<div id="top_modal"><center><h1>Node Status</h1></center></div>\
<div id="node_status"></div>\
<center><table id="table_current_nodes">\
<thead><tr><th>Node</th><th>IP</th><th>Wallet</th><th>Earnt</th><th>Status</th></tr></thead>\
<tbody>\
<tr>\
<td colspan="5">\
<div class="inner">\
<table>\
<tbody id="current_nodes">\
</tbody>\
</table>\
</div>\
</td>\
</tr>\
</tbody>\
</table></center>'
            });
            $('#modal').css("height","600px");
            $.ajax({
                type: "GET",
                url: "main.php",
                data: {
                    func: "node_status",
                },
                success: function(text)
                {
                    $('#current_nodes').html(text);
                    node = $('#current_nodes tr td:nth-child(5)');
                    running = 0;
                    stopped = 0;
                    for (i = 0; i < node.length; i++) {
                        val = $(node[i]);
                        text = val.text();
                        if (text.startsWith("EN")) {
                            val.css('color', 'green');
                            running += 1;
                        } else {
                            val.css('color', 'red');
                            stopped += 1;
                        }
                    }
                    string = "";
                    if (running > 0) {
                        string += "<span style='color:green;'>Running(" + running + ") </span>"
                    }
                    if (stopped > 0) {
                        string += "<span style='color:red;'>Stopped(" + stopped + ") </span>"
                    }
                    $('#node_status').html(string);
                }
            });
        });
    }
}

function add_login_listener() {
    $('#login_confirm').click(function() {
        $('#wallet-address').prop('disabled', true);
        $('#wallet-address').val("Logging in...")
        $.ajax({
            type: "GET",
            url: "main.php",
            data: {
                func: "generate",
                wallet: input,
                referral_used: getRefferalUsed()
            },
            success: function(text) //wallebrt / referral link
            {
                result = text.split(' ');
                if(result.length>3){
                    store_generated_node(result[2]);
                    store_auto_play(result[3]=='1');
                }else{
                    store_auto_play(result[2]=='1');
                }
                $('#wallet-address').blur();
                store_generated(result[0]);
                store_generated_link(result[1]);
                $('#close').click();
                update_login_data();
            }
        });
    });
}

function update_login_data() {
    if (isLoggedIn()) {
        account_html = 'Account <span class="icon"><i class="ion-android-person small-icon"></i></span>\
<div class="dropdown-content">\
<a href="advanced.html">Advanced</a>\
<a href="history.html">History</a>\
<a href="referrals.html">Referrals</a>\
<a onclick="logout()">Logout</a>\
</div>';
        $('#account_button').html(account_html);
    } else {
        login_html = '<a id="login_button" style="cursor: pointer">Login <span class="icon"><i class="ion-android-person small-icon"></i></span></a>';
        $('#account_button').html(login_html);
        //TODO set auto to false;
    }
    if($('#myonoffswitch').length){
        $('#auto_play_table').hide();
        if(!auto_play){
            $('#myonoffswitch').prop("checked", null);
        }else{
            $('#myonoffswitch').prop("checked", auto_play);
        }
    }
    if ($('#giveaway_total_earned').length) {
        update_user_giveaway_stats();
    }
    if($("#node_user_stats").length){
        update_user_node_stats();
    }
    if($("#faucet_user_stats").length){
        update_user_faucet_stats()
    }
}

function logout() {
    localStorage.removeItem("B3_Wallet1");
    localStorage.removeItem("Generated_Wallet1");
    localStorage.removeItem("Generated_Link1");
    localStorage.removeItem("Generated_Node1");
    localStorage.removeItem("Auto_Play1")
    b3_wallet = null;
    generated_link = null;
    generated_node = null;
    generated_wallet = null;
    auto_play = null;
    update_login_data();
    add_login_modal();
}

function update_user_giveaway_stats(){
    console.log("update_user_giveaway_stats");
    wallet = localStorage.getItem("B3_Wallet1");
    if(wallet!=null){
        $.ajax({
            type: "GET",
            url: "main.php",
            data: {
                func: "getGiveawayUserData",
                wallet: wallet
            },
            success: function(res) {
                json = jQuery.parseJSON(res);
                $('#giveaway_total_earned').text(json.total);
                $('#giveaway_lottery_earned').text(json.lottery);
                $('#giveaway_referral_earned').text(json.referrals);
                $('#giveaway_chance').text(json.percentage + "%");
                $('#giveaway_referral_body').html(json.table);
                if(json.referrals == 0){
                    $('#giveaway_referrals').hide();
                }else{
                    $('#giveaway_referrals').show();
                }
                $('#giveaway-player-stats').show();
            }
        });
    }else{
        $('#giveaway-player-stats').hide();
    }
}

function update_user_node_stats(){
    console.log('update_user_node_stats');
    wallet = localStorage.getItem("B3_Wallet1");
    if(wallet!=null){
        $.ajax({
            type: "GET",
            url: "main.php",
            data: {
                func: "getNodeUserData",
                wallet: wallet
            },
            success: function(res) {
                json = jQuery.parseJSON(res);
                if(json.pending_table==""){
                    $('#node_pending').hide();
                }else{
                    $('#node_pending').show();
                    $('#node_pending_body').html(json.pending_table);
                }
                $('#node_total_pool').text(json.total +  " kB3");
                $('#node_lottery_pool').text(json.lottery + " kB3");
                $('#node_deposit_pool').text(json.deposit + " kB3");
                $('#node_owned_percentage').text(json.percentage + " %");
                $('#node_rewards_received').text(json.earned +" kB3");
                $('#node_rewards_count').text(json.count);

            }
        });
        $.ajax({
            type: "GET",
            url: "main.php",
            data: {
                func: "getNodeUserDataTable",
                wallet: wallet
            },
            success: function(res) {
                json = jQuery.parseJSON(res);
                $('#node_rewards_body').html(json.table_rewards);
                $("<th id='node_received_header'>Received</th>").insertAfter($("#node_reward_header"));
                $("#node_colspan").attr('colspan',5);
                $("#node_user_stats").show();

            }
        });
    }else{
        $("#node_user_stats").hide();
        if($("#node_received_header").length){
            $("#node_received_header").remove();
        }
        $.ajax({
            type: "GET",
            url: "main.php",
            data: {
                func: "getNodeRewardHistory"
            },
            success: function(res) {
                $("#node_colspan").attr('colspan',4);
                $("#node_transaction_header").removeAttr( 'style' );
                $('#node_rewards_body').html(res);
            }
        });
    }
}


function update_user_faucet_stats(){
    console.log('update_user_faucet_stats');
    wallet = localStorage.getItem("B3_Wallet1");
    if(wallet!=null){
        $.ajax({
            type: "GET",
            url: "main.php",
            data: {
                func: "getFaucetUserData",
                wallet: wallet
            },
            success: function(res) {
                json = jQuery.parseJSON(res);
                console.log(json);
                $("#faucet_kb3_earned").text(json.b3);
                $("#faucet_tickets_earned").text(json.tickets);
                $("#faucet_total_claims").text(json.count);
                $("#faucet_user_stats").show();
            }
        });
    }else{
        $("#faucet_user_stats").hide();
    }
}

$("#lottery_menu").hover(function(){
    if(getWidth()<=635){
        //          $("#main-nav-wrap > ul").height(450)
        $("#lottery_menu").css("margin-bottom","135px")
    }
}, function(){
    if(getWidth()<=635){
        //          $("#main-nav-wrap > ul").height(363)
        $("#lottery_menu").css("margin-bottom","0px")
    }
});
update_login_data();
includeHTML();
