<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

 require_once('easybitcoin.php');
require_once("solvemedialib.php");


function get_db(){
    ini_set ('error_reporting', E_ALL);
    ini_set ('display_errors', '1');
    error_reporting (E_ALL|E_STRICT);

    $db = mysqli_init();
    mysqli_options ($db, MYSQLI_OPT_SSL_VERIFY_SERVER_CERT, true);

    $db->ssl_set('certs/client-key.pem', 'certs/client-cert.pem', 'certs/server-ca.pem', NULL, NULL);
    return $db;
}

function get_db_value($string){
    $db = get_db();
    $link = mysqli_real_connect ($db, 'mysql_server', 'AltFreq', 'Ms016356!01', 'b3_lottery', 3306, NULL, MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT);
    if (!$link)
    {
        return "Error with database";
    } else {
        $res = $db->query("$string;");
        $db->close();
        return $res;
    }
}


function set_db_value($string){
    $db = get_db();
    $link = mysqli_real_connect ($db, 'mysql_server', 'AltFreq', 'Ms016356!01', 'b3_lottery', 3306, NULL, MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT);
    if (!$link)
    {
        return "Error with database";
    } else {
        $res = $db->query("$string;");
        $db->close();
    }
}

function sql_multi_query($string){
    $db = get_db();
    $link = mysqli_real_connect ($db, 'mysql_server', 'AltFreq', 'Ms016356!01', 'b3_lottery', 3306, NULL, MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT);
    if (!$link)
    {
        return "Error with database";
    } else {
        $res = $db->multi_query("$string;");
        $db->close();
    }
}

function getTicketsSold(){
    $string = "SELECT Tickets_Sold FROM game_round ORDER BY ID DESC LIMIT 1";
    return getSQLValue($string);
}


$GLOBALS['tickets_sold'] = getTicketsSold();
$GLOBALS['ticket_cost'] = 10;
$GLOBALS['fn_value'] = getTotalFNValueSQL();
$GLOBALS['rpc'] = setupRPC();

$GLOBALS['round_max_ID'] = null;

$GLOBALS['node_kb3_amount'] = null;

$GLOBALS['giveaway_amount'] = null;
$GLOBALS['total_giveaway_tickets'] = null;
$GLOBALS['giveaway_max_id'] = null;

function getSQLNodeKB3Amount(){
    $string = "SELECT SUM(Amount)+SUM(Direct_Deposit)+IFNULL((SELECT SUM(Amount)*0.25 FROM node_deposit_pending),0) FROM node_owners";
    return getSQLValue($string);
}

function getNodeKB3Amount(){
    $amount = $GLOBALS['node_kb3_amount'];
    if($amount == null){
        $amount = getSQLNodeKB3Amount();
        $GLOBALS['node_kb3_amount'] = $amount;
    }
    return $amount;
}

function getSQLRoundMAXID(){
    $string = "SELECT MAX(ID) FROM game_round";
    return getSQLValue($string);
}

function getRoundMAXID(){
    $id = $GLOBALS['round_max_ID'];
    if($id == null){
        $id = getSQLRoundMAXID();
        $GLOBALS['round_max_ID'] = $id;
    }
    return $id;
}

function getSQLGiveawayMAXID(){
    $string = "SELECT MAX(ID) FROM daily_giveaway";
    return getSQLValue($string);
}

function getGiveawayMAXID(){
    $id = $GLOBALS['giveaway_max_id'];
    if($id == null){
        $id = getSQLGiveawayMAXID();
        $GLOBALS['giveaway_max_id'] = $id;
    }
    return $id;
}

function getSQLGiveawayAmount(){
    $string = "SELECT Amount FROM daily_giveaway ORDER BY ID DESC LIMIT 1";
    return getSQLValue($string);
}

function getGiveAwayAmount(){
  $amount = $GLOBALS['giveaway_amount'];
  if($amount == null){
    $amount = getSQLGiveawayAmount();
    $GLOBALS['giveaway_amount'] = $amount;
  }
  return $amount;
}

function getTotalGiveawayTickets(){
    $amount = $GLOBALS['total_giveaway_tickets'];
  if($amount == null){
    $amount = getSQLTotalGiveAwayTickets();
    $GLOBALS['total_giveaway_tickets'] = $amount;
  }
  return $amount;
}

function setupRPC(){
    $b3coin = new Bitcoin('AltFreq','Ms016356!01','localhost','9001');
    return $b3coin;
}

function getSQLValue($sql){
    $res = get_db_value($sql);
    if(!$res){
        return null;
    }
    while($cRow = mysqli_fetch_array($res))
    {
        return $cRow[0];
    }
}



function getTotalPaidHistory(){
    $string = "SELECT SUM(Amount ) + (SELECT SUM(Amount) from giveaway_payout_history) + (SELECT SUM(Amount) FROM node_payouts_history) + (SELECT SUM(Amount) FROM faucet_payment_history) FROM payment_history";
    return getSQLValue($string);
}

function printTotalPaidHistory(){
    echo number_format(getTotalPaidHistory(),2);
}

function getTotalNodeOwners(){
    $string = "SELECT COUNT(Wallet_Address) FROM node_owners";
    return getSQLValue($string);
}

function printTotalNodeOwners(){
    echo number_format(getTotalNodeOwners());
}

function getCurrentRoundID(){
    $string = "SELECT ID FROM game_round ORDER BY ID DESC LIMIT 1";
    return getSQLValue($string);
}

function printCurrentRoundID(){
    echo number_format(getCurrentRoundID());
}

function getCurrentRoundTickets(){
    return $GLOBALS['tickets_sold'];
}

function getRPC(){
    return $GLOBALS['rpc'];
}

function printCurrentRoundTickets(){
    echo number_format(getCurrentRoundTickets());
}

function getTicketCost(){
    return $GLOBALS['ticket_cost'];
}

function getTotalFNValue(){
    return $GLOBALS['fn_value'];
}

function getCurrentRoundTotal(){
    $cost = getCurrentRoundTickets()*getTicketCost();
    return round($cost,5);
}


function printCurrentRoundTotal(){
    print number_format(getCurrentRoundTotal());
}

function getCurrentRoundWinnersPool(){
    $total = getCurrentRoundTotal();
    $val = $total *0.9;
    if($val <= 90){
        return 90;
    }else{
        return round($val,5);
    }
}

function printCurrentRoundWinnersPool(){
    print number_format(getCurrentRoundWinnersPool() + getCurrentRoundLosersPool() - getCurrentRoundNodePool()*1.25);
}

function getCurrentRoundLosersPool(){
    $total = getCurrentRoundTotal();
    if($total < 100){
        $total = 100;
    }
    $win = getCurrentRoundWinnersPool();
    $loser = ($total-$win);
    return round($loser,5);
}

function printCurrentRoundLosersPool(){
    print getCurrentRoundLosersPool();
}

function getCurrentRoundNodePool(){
    return round(getCurrentRoundLosersPool() * 0.09 *0.75,5);
}

function printCurrentRoundNodePool(){
    print number_format(getCurrentRoundNodePool());
}

function getWinnersCount(){
    $tickets = getCurrentRoundTickets();
    return round($tickets*0.9,0);
}

function getLosersCount(){
    $winners = getWinnersCount();
    $total = getCurrentRoundTickets();
    return $total-$winners;
}

function getGroupOneSize(){
    $tickets = getCurrentRoundTickets();
    if($tickets < 10){
        $tickets = 10;
    }
    return round($tickets*0.1,0);
}

function getGroupTwoSize(){
    $tickets = getCurrentRoundTickets();
    if($tickets < 10){
        $tickets = 10;
    }
    return round($tickets*0.1,0);
}

function getGroupThreeSize(){
    $tickets = getCurrentRoundTickets();
    if($tickets < 10){
        $tickets = 10;
    }
    return round($tickets*0.2,0);
}

function getGroupFourSize(){
    $tickets = getCurrentRoundTickets();
    if($tickets < 10){
        $tickets = 10;
    }
    return round($tickets*0.25,0);
}

function getGroupFiveSize(){
    $winners = getWinnersCount();
    if($winners < 9){
        $winners = 9;
    }
    $sum = getGroupOneSize() + getGroupTwoSize() +  getGroupThreeSize() + getGroupFourSize();
    return $winners-$sum;
}

function getGroupOneWinnings(){
    $cost = getTicketCost(); //10
    $losers = getCurrentRoundLosersPool();
    $lose_percent = $losers*(0.40/getGroupOneSize());
    return number_format(round($cost+$lose_percent, 5, PHP_ROUND_HALF_DOWN),5);
}

function getGroupTwoWinnings(){
    $cost = getTicketCost();
    $losers = getCurrentRoundLosersPool();
    $lose_percent = $losers*(0.25/getGroupTwoSize());
    return number_format(round($cost+$lose_percent, 5, PHP_ROUND_HALF_DOWN),5);
}

function getGroupThreeWinnings(){
    $cost = getTicketCost();
    $losers = getCurrentRoundLosersPool();
    $lose_percent = $losers*(0.12/getGroupThreeSize());
    return number_format(round($cost+$lose_percent, 5, PHP_ROUND_HALF_DOWN),5);
}

function getGroupFourWinnings(){
    $cost = getTicketCost();
    $losers = getCurrentRoundLosersPool();
    $lose_percent = $losers*(0.1/getGroupFourSize());
    return number_format(round($cost+$lose_percent, 5, PHP_ROUND_HALF_DOWN),5);
}

function getGroupFiveWinnings(){
    $cost = getTicketCost();
    $losers = getCurrentRoundLosersPool();
    $lose_percent = $losers*(0.04/getGroupFiveSize());
    return round($cost+$lose_percent, 5, PHP_ROUND_HALF_DOWN);
}

function numberToPlace($number){
    $lastDigit = substr("$number", -1);
    if($lastDigit >3 OR $lastDigit == 0){
        return number_format($number) . "th";
    }else if ($lastDigit == 1){
        return number_format($number) . "st";
    }else if ($lastDigit == 2){
        return number_format($number) . "nd";
    }else if ($lastDigit == 3){
        return number_format($number) . "rd";
    }
}

function getGroupOnePrint(){
    $size = getGroupOneSize(); //1-5
    $start = 1;
    $finish = $size;
    if($size==1){
        return numberToPlace($start);
    }else{
        return numberToPlace($start) . " - " . numberToPlace($finish);
    }
}

function getGroupTwoPrint(){
    $size = getGroupTwoSize(); //2
    $g1 = getGroupOneSize(); //1
    $finish = $size+$g1;
    $start = $finish - $size +1 ;
    if($size==1){
        return numberToPlace($start);
    }else{
        return numberToPlace($start) . " - "  . numberToPlace($finish);
    }
}

function getGroupThreePrint(){
    $size = getGroupThreeSize(); //2
    $g1 = getGroupOneSize(); //1
    $g2 = getGroupTwoSize(); //2
    $finish = $size+$g1+$g2; //5
    $start = $finish - $size +1;
    if($size==1){
        return numberToPlace($start);
    }else{
        return numberToPlace($start) . " - "  . numberToPlace($finish);
    }
}

function getGroupFourPrint(){
    $size = getGroupFourSize(); //3
    $g1 = getGroupOneSize(); //1
    $g2 = getGroupTwoSize(); //2
    $g3 = getGroupThreeSize();//2
    $finish = $size+$g1+$g2+$g3; //8
    $start = $finish - $size + 1;
    if($size==1){
        return numberToPlace($start);
    }else{
        return numberToPlace($start) . " - "  . numberToPlace($finish);
    }
}

function getGroupFivePrint(){
    $size = getGroupFiveSize(); //1
    $g1 = getGroupOneSize(); //1
    $g2 = getGroupTwoSize(); //2
    $g3 = getGroupThreeSize();//2
    $g4 = getGroupFourSize();//3
    $finish = $size+$g1+$g2+$g3+$g4; //9
    $start = $finish - $size +1;
    if($size==1){
        return numberToPlace($start);
    }else{
        return numberToPlace($start) . " - "  . numberToPlace($finish);
    }
}

function getNodeGroupSize(){
    $losers = getLosersCount();
    if($losers <= 0){
        return 1;
    }else{
        return $losers;
    }
}

function getNodeGroupWinnings(){
    $size = getNodeGroupSize();
    $node_val = getCurrentRoundNodePool();
    $node_each = round($node_val/$size,5);
    return number_format($node_each,5)." kB3";
}

function getNodeGroupPrint(){
    $size = getNodeGroupSize();
    $g1 = getGroupOneSize(); //1
    $g2 = getGroupTwoSize(); //2
    $g3 = getGroupThreeSize();//2
    $g4 = getGroupFourSize();//3
    $g5 = getGroupFiveSize();
    $finish = $size+$g1+$g2+$g3+$g4+$g5; //9
    $start = $finish - $size +1;
    if($size==1){
        return numberToPlace($start);
    }else{
        return numberToPlace($start) . " - "  . numberToPlace($finish);
    }
}

function getBasedOnTickets(){
    $tickets = getCurrentRoundTickets();
    if($tickets < 10){
        return 10;
    }else{
        return $tickets;
    }
}

function getPendingDesposits(){
    $sql = ("SELECT Wallet_Address, FLOOR(SUM(Deposit_Amount)/10) FROM pending_deposits GROUP BY Wallet_Address");
    $res = get_db_value($sql);
    return $res;
}

function getCompletedDeposits(){
    $sql = ("SELECT Wallet_Address, FLOOR(SUM(Deposit_Amount)/10) FROM completed_deposits WHERE Round_ID = ".getRoundMAXID()." GROUP BY Wallet_Address");
    $res = get_db_value($sql);
    return $res;
}

function getAutoTickets(){
    $sql = ("SELECT wallet, FLOOR(SUM(balance)/10) FROM auto_balances WHERE balance >=10 GROUP BY wallet");
    $res = get_db_value($sql);
    return $res;
}


function getCurrentDeposits(){
    $pending = getPendingDesposits();
    $completed = getCompletedDeposits();
    $auto = getAutoTickets();
    $wallets = array();
    while($row = mysqli_fetch_array($pending))
    {
        $wallet = $row[0];
        $tickets = $row[1];
        $wallets[$wallet][0] = $tickets;
    }
    while($row = mysqli_fetch_array($completed))
    {
        $wallet = $row[0];
        $tickets = $row[1];
        $wallets[$wallet][1]= $tickets;
    }
    while($row = mysqli_fetch_array($auto))
    {
        $wallet = $row[0];
        $tickets = $row[1];
        $wallets[$wallet][2]= $tickets;
    }
    return $wallets;
}

function getCurrentRoundTable(){
    $array = getCurrentDeposits();
    foreach($array as $key=>$value){
        $string = " <tr><td><b>" . $key . "</b></td><td>";
        $pending = 0;
        $completed = 0;
        $auto = 0;
        if(isset($value[0])){
            $pending = $value[0];
        }
        if(isset($value[1])){
            $completed = $value[1];
        }
        if(isset($value[2])){
            $auto = $value[2];
        }
        if($auto >0){
            $string = $string . "<b style='color:green'>" . number_format($auto) . " </b>";
        }
        if($completed > 0){
            $string = $string . "<b>" . number_format($completed) . "</b>";
        }
        if($pending > 0){
            $string = $string . "(" . number_format($pending) . ") ";
        }
        $string = $string . "</td></tr>";
        echo $string;
    }

}

function getCurrentRoundTimeLeft(){
    $string = "SELECT End FROM game_round ORDER BY ID DESC LIMIT 1";
    $res = getSQLValue($string);
    $date = strtotime($res);
    $diff = $date-time();
    $seconds = $diff % 60;
    $minutes = $diff / 60 % 60;
    $hours = $diff /3600;
    if($hours <= 0 AND $minutes <= 0 AND $seconds <= 0){
        print "Waiting for block to be found";
    }else{
        $format = sprintf("%d:%02d:%02d", $hours, $minutes, $seconds);
        print $format;
    }
}




function getSQLTotalGiveAwayTickets(){
    $sql ="SELECT IFNULL(SUM(lottery_tickets)+SUM(referral_tickets),0) FROM giveaway_tickets WHERE giveaway_id = (SELECT MAX(ID) FROM daily_giveaway)";
    $val = getSQLValue($sql);
    return $val;
}

function printTotalGiveAwayTickets(){
    echo number_format(getTotalGiveawayTickets());
}

function getPlayerTickets($b3wallet){
    $sql = "SELECT SUM(Deposit_Amount)/10 from completed_deposits WHERE completed_deposits.Round_ID = ".getRoundMAXID()." AND Wallet_Address ='$b3wallet'";
    return getSQLValue($sql);
}

function printPlayerTickets($b3wallet){
    return number_format(getPlayerTickets($b3wallet));
}

function getIP(){
        $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function getFaucetCollect(){
    $sql = "SELECT available from faucet_balance ORDER BY Date DESC LIMIT 1";
    $val =getSQLValue($sql);
    if($val <= 0){
        return 0.0;
    }
    $rand = mt_rand(0,100);
    if($rand>70){
        return 0.1;
    }else if($rand>50){
        return 0.2;
    }else if($rand>30){
        return 0.5;
    }else if ($rand>20){
        return 1.0;
    }else if ($rand>15){
        return 1.5;
    }else if ($rand>5){
        return 2.0;
    }else {
        return 10.0;
    }
}

function captchaSolve(){
    $cap_privkey="wfoZBsW96-5qlgnSLZM6o7nBpc3iIP9y";
    $cap_hashkey="sH1lCL--7Ll1lR3IpPAgpql0a7eSpLA7";
    $valid = false;

    if(isset($_POST["adcopy_challenge"])){
        $solvemedia_response = solvemedia_check_answer($cap_privkey,
                        $_SERVER["REMOTE_ADDR"],
                        $_POST["adcopy_challenge"],
                        $_POST["adcopy_response"],
                        $cap_hashkey);
        if (!$solvemedia_response->is_valid) {
            print "Error: ".$solvemedia_response->error;
            return;
        }else{
            $valid=true;
        }
    }
    else{
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = array(
            'secret' => '6LfOXEwUAAAAAMb4HL7XgTb2iPxF7luYuM6tkvEy',
            'response' => $_POST["g_recaptcha_response"]
        );

        $options = array(
            'http' => array(
                'method' => "POST",
                'header' =>
                    "Content-Type: application/x-www-form-urlencoded\r\n".
                    "Authorization: Bearer sdf541gs6df51gsd1bsb16etb16teg1etr1ge61g\n",
                'content' => http_build_query($data)
        ));

        $context  = stream_context_create($options);
        $verify = file_get_contents($url, false, $context);
        $captcha_success=json_decode($verify);
        if ($captcha_success->success==false) {
            print "Error Code " . $captcha_success->error-codes;
            return;
        } else if ($captcha_success->success==true) {
            $valid= true;
        }
    }

    if($valid) {
        $ip = getIP();
        $b3_wallet = $_POST["b3_wallet"];
        if(empty($b3_wallet)){
            print "Error: Not logged in.";
            return;
        }
        $sql = "SELECT last_claim FROM faucet_claims where ip_address='$ip' or b3_wallet='$b3_wallet' ORDER BY last_claim DESC limit 1";
        $res = get_db_value($sql);
        $val=0;

        while($row = mysqli_fetch_array($res)){
            $val=$row[0];
        }
        $hours =0;
        if(empty($val) || $val==null){
            $hours =7;
        }else{
            $now = new DateTime();
            $post = new DateTime($val);
            $interval = $now->diff($post);
            $hours = $interval->h + ($interval->format("%a") * 24);
        }
        if($hours >= 6){
            $fac = getFaucetCollect();
            $tick = 0;
            $b3_won = 0;
            if($fac == 10.0){
                $tick=1;
                $sql = "INSERT INTO faucet_claims (b3_wallet, ip_address, last_claim, claim_count, tickets_won, b3_won) VALUES ('$b3_wallet', '$ip', UTC_TIMESTAMP(), 1, $tick, $b3_won); UPDATE faucet_balance set available=available-$fac order by date desc limit 1; INSERT INTO completed_deposits (Tx_Number, Wallet_Address, Round_ID, Deposit_Amount) VALUES (CONCAT('faucet ',(SELECT MAX(id)+1 from faucet_claims),'$b3_walletb3'),'$b3_wallet', (SELECT MAX(ID) FROM game_round) ,$fac)";
            }else{
                $b3_won=$fac;
                $sql = "INSERT INTO faucet_claims (b3_wallet, ip_address, last_claim, claim_count, tickets_won, b3_won) VALUES ('$b3_wallet', '$ip', UTC_TIMESTAMP(), 1, $tick, $b3_won); UPDATE faucet_balance set available=available-$fac order by date desc limit 1; INSERT INTO faucet_pending (wallet_address, amount) VALUES ('$b3_wallet', $fac)";
            }
            sql_multi_query($sql);

            if($tick==0){
                print 'Success You have been rewarded ' . $b3_won . ' kB3';
            }else{
                print 'Success You have been rewarded ' . 1 . ' ticket in the lottery! Click <a href="active.html">here</a> to view the current round';
            }
        }else{
            print 'Error: You must wait atleast 6 hours before you can claim again. ';
        }
//        print 'success' . $val;
    }
}

function updateActivePage(){
    $table = "<tr><td>".getGroupOnePrint()."</td><td>".getGroupOneWinnings()."kB3</td></tr>
                        <tr><td>".getGroupTwoPrint()."</td><td>".getGroupTwoWinnings()."kB3</td></tr>
                        <tr><td>".getGroupThreePrint()."</td><td>".getGroupThreeWinnings()." kB3</td></tr>
                        <tr><td>".getGroupFourPrint()."</td><td>".getGroupFourWinnings()." kB3</td></tr>
                        <tr><td>".getGroupFivePrint()."</td><td>".getGroupFiveWinnings()." kB3</td></tr>
                        <tr><td>".getNodeGroupPrint()."</td><td><span style='color:#737373;'>FN Pool </span>".getNodeGroupWinnings()."</td></tr>";
    return json_encode(array("tickets" => printCurrentRoundTickets(), "total" => printCurrentRoundTotal(), "winnerspool" => printCurrentRoundWinnersPool(), "nodepool" => printCurrentRoundNodePool(),
                             "groups_table" => $table, "current_table" => getCurrentRoundTable(), "player_tickets" => printPlayerTickets($_GET['b3wallet'])));
}

function toggle_auto_play($wallet){
    $sql = "UPDATE wallet_addresses SET auto_play = !auto_play WHERE b3_wallet='$wallet'";
    set_db_value($sql);
    $sql = "SELECT auto_play from wallet_addresses where b3_wallet='$wallet'";
    $res = getSQLValue($sql);
    return $res;
}


if(isset($_GET['func'])) {
    if($_GET['func'] == 'getPlayerTickets') {
        echo printPlayerTickets($_GET['b3wallet']);
    }else if($_GET['func'] == 'updateActivePage'){
        echo updateActivePage();
    }else if($_GET['func'] == 'getGiveawayUserData'){
        echo getGiveawayUserData($_GET['wallet']);
    }else if($_GET['func'] == 'getNodeUserData'){
        echo getNodeUserData($_GET['wallet']);
    }else if($_GET['func'] == 'getNodeUserDataTable'){
        echo getNodeUserDataTable($_GET['wallet']);
    }else if($_GET['func'] == 'getFaucetUserData'){
        echo getFaucetUserData($_GET['wallet']);
    }else if($_GET['func'] == 'generate') {
        echo generateAddress($_GET['wallet'], $_GET['referral_used']);
    }else if($_GET['func'] == 'auto_play'){
        echo toggle_auto_play($_GET['wallet']);
    }else if($_GET['func'] == 'node_status') {
        echo getRunningNodesTable();
    }else if($_GET['func'] == 'getPreviousRound'){
        echo getPreviousRound($_GET['roundID']);
    }else if($_GET['func'] == 'getPreviousGiveaway'){
        echo getPreviousGiveaway($_GET['roundID']);
    }else if($_GET['func'] == 'gen_node'){
        echo generateNodeAddress($_GET['wallet']);
    }else if($_GET['func'] == 'getNodeRewardHistory'){
        echo getNodeRewardHistory();
    }else{
        echo "FML";
    }
}

if(isset($_POST['func'])){
    if($_POST['func']== 'captcha'){
                captchaSolve();
    }
}

function generateNodeAddress($b3wallet){
    $opts = array(
      'ssl' => array(
          'local_cert' => '/etc/apache2/ssl/cert',
          'local_pk' => '/etc/apache2/ssl/key',
          'verify_peer' => true,
          )
      );

    $timeout = 10;
    $host = "main_wallet:9002";

    $context = stream_context_create($opts);
    $socket = stream_socket_client (
      $host, $errno, $errstr, $timeout,
      STREAM_CLIENT_CONNECT, $context);
    if ($socket) {
        $sent = stream_socket_sendto($socket, $b3wallet . " none true");
        if ($sent > 0) {
            $server_response = fread($socket, 4096);
            return $server_response;
        }
    } else {
        return 'Unable to connect to server';
    }
}


function generateAddress($b3wallet, $referral_used){
    $opts = array(
      'ssl' => array(
          'local_cert' => '/etc/apache2/ssl/cert',
          'local_pk' => '/etc/apache2/ssl/key',
          'verify_peer' => true,
          )
      );

    $timeout = 10;
    $host = "main_wallet:9002";

    $context = stream_context_create($opts);
    $socket = stream_socket_client (
      $host, $errno, $errstr, $timeout,
      STREAM_CLIENT_CONNECT, $context);
    if($referral_used == ' '){
        $referral_used = 'none';
    }
    if ($socket) {
        $sent = stream_socket_sendto($socket, $b3wallet . " " . $referral_used);
        if ($sent > 0) {
            $server_response = fread($socket, 4096);
            return $server_response;
        }
    } else {
        return 'Unable to connect to server';
    }
}

function getTopWinners(){
    $sql = ("SELECT Wallet_Address, SUM(Win_Total) as Win FROM history_player GROUP BY Wallet_Address ORDER BY Win DESC LIMIT 10");
    $res = get_db_value($sql);
    $i = 1;
    while($row = mysqli_fetch_array($res))
    {
        echo "<tr><td>#$i</td><td>$row[0]</td><td>" . number_format(round($row[1],5),5) . "</td></tr>";
        $i++;
    }
}

function getTopLosers(){
    $sql = ("SELECT Wallet_Address, SUM(Lose_Total) as Lose FROM history_player GROUP BY Wallet_Address ORDER BY Lose DESC LIMIT 10");
    $res = get_db_value($sql);
    $i = 1;
    while($row = mysqli_fetch_array($res))
    {
        echo "<tr><td>#$i</td><td>$row[0]</td><td>" . number_format(round($row[1],5),5) . "</td></tr>";
        $i++;
    }
}

function getTotalFNValueSQL(){
    $sql = "SELECT SUM(Cost) FROM nodes";
    return getSQLValue($sql);
}

function getLastRound(){
    $sql = ("SELECT Wallet_Address, Tickets, Win_Total, Lose_Total FROM history_player WHERE Round_ID = (".getRoundMAXID()."-1) ORDER BY Win_Total DESC");
    $res = get_db_value($sql);
    while($row = mysqli_fetch_array($res))
    {
        echo "<tr><td>$row[0]</td><td>" . number_format($row[1])."</td><td>" . number_format(round($row[3],5),5) . " (" . number_format(round($row[3]/getTotalFNValue()*100,5),5) ."%)</td><td>".number_format(round($row[2],5),5)."</td></tr>";
    }
}


function getLast50Payments(){
    $sql = ("SELECT Wallet_Address, Amount, Transaction_ID FROM payment_history ORDER BY ID DESC LIMIT 50");
    $res = get_db_value($sql);
    while($row = mysqli_fetch_array($res))
    {
        echo "<tr><td>Tx ID: <a href='https://chainz.cryptoid.info/b3/tx.dws?$row[2]' target='_blank'>$row[2]</a><br>Wallet: $row[0]</td><td>" . number_format(round($row[1],5),5) . "</td></tr>";
    }
}

function checkReferral(){
    if (isset($_GET['r'])){
        return "<div id='referral-code' style='display:none'>" . $_GET['r'] . "</div>";
    }
    return "";
}

function getPreviousRoundTickets(){
    $sql = "SELECT Tickets_Sold FROM game_round ORDER BY ID DESC LIMIT 1,1";
    return getSQLValue($sql);
}

function printPreviousRoundTickets(){
    echo number_format(getPreviousRoundTickets());
}

function getPreviousRoundID(){
    $sql = "SELECT MAX(ID)-1 FROM game_round";
    return getSQLValue($sql);
}

function printPreviousRoundID(){
    echo number_format(getPreviousRoundID());
}

function getPreviousRoundTicketsID($id){
        $sql = "SELECT Tickets_Sold FROM game_round WHERE ID=$id";
    return getSQLValue($sql);
}

function getPreviousRound($id){
    $sql = "SELECT Wallet_Address, Tickets, Win_Total, Lose_Total FROM history_player WHERE Round_ID = $id ORDER BY Win_Total DESC";
    $res = get_db_value($sql);
    $string = "";
    while($row = mysqli_fetch_array($res))
    {
        $string = $string . "<tr><td>$row[0]</td><td>" . number_format($row[1]) . "</td><td>" . number_format(round($row[3],5),5) . " (" . number_format(round($row[3]/getTotalFNValue()*100,5),5) ."%)</td><td>".number_format(round($row[2],5),5) . "</td></tr>";
    }
    return json_encode(array("table" => $string, "tickets" => number_format(getPreviousRoundTicketsID($id))));
}

function getPreviousRoundLinks(){
    $sql = ("SELECT ID FROM game_round ORDER BY ID DESC LIMIT 1,21");
    $res = get_db_value($sql);
    $string = "";
    $i=0;
    while($row = mysqli_fetch_array($res))
    {
        $string = $string . "<a href='' hist=$row[0]>$row[0]</a>";
        if($i!=20){
            $string = $string . ", ";
        }
        $i++;
    }
    return $string;
}

function getPreviousGiveaway($id){
    $sql = "SELECT start, amount, total_tickets FROM daily_giveaway WHERE ID = $id";
    $res = get_db_value($sql);
    $string = "";
    $array = array();
    $i=1;
    while($row = mysqli_fetch_array($res))
    {
     $array += ["date" => date('F j, Y', strtotime($row[0])), "amount" => number_format($row[1],2), "total_tickets" => number_format($row[2])];
    }
    $sql = "SELECT Wallet_Address, Amount, Tickets FROM giveaway_payout_history WHERE giveaway_ID = $id ORDER BY Amount DESC";
    $res = get_db_value($sql);
    while($row = mysqli_fetch_array($res))
    {
        $string = $string . "<tr><td>".numberToPlace($i)."</td><td>$row[0]</td><td>" . number_format($row[2]) . "</td><td>" . number_format($row[1]) . " kB3</td></tr>";
        $i++;
    }
    $array += ["table" => $string];
    return json_encode($array);
}

function getPreviousGiveawayLinks(){
    $sql = ("SELECT ID FROM daily_giveaway ORDER BY ID DESC LIMIT 1,21");
    $res = get_db_value($sql);
    $string = "";
    $i=0;
    while($row = mysqli_fetch_array($res))
    {
        $string = $string . "<a href='' hist=$row[0]>$row[0]</a>";
        if($i!=20){
            $string = $string . ", ";
        }
        $i++;
    }
    return $string;
}

function getUniquePlayers(){
    $sql = "SELECT COUNT(distinct Wallet_Address) from completed_deposits";
    return getSQLValue($sql);
}

function printUniquePlayers(){
    echo number_format(getUniquePlayers());
}

function getLotteryPayout(){
    $sql = "select SUM(Win_Total) from history_player";
    return getSQLValue($sql);
}

function printLotteryPayout(){
    echo number_format(getLotteryPayout(),3);
}

function getNodePayouts(){
    $sql = "select SUM(Amount) from node_payouts_history";
    return getSQLValue($sql);
}

function printNodePayouts(){
    echo number_format(getNodePayouts(),3);
}

function getWinLossRatio(){
    $sql = "SELECT (SUM(Tickets)-FLOOR(SUM(Lose_Total)/0.16))/FLOOR(SUM(Lose_Total)/0.16) FROM history_player";
    return round(getSQLValue($sql),3);
}

function printWinLossRatio(){
    echo number_format(getWinLossRatio(),2);
}


function getTotalTicketsSold(){
    $sql = "SELECT SUM(Tickets_Sold) FROM game_round";
    return getSQLValue($sql);
}


function printTotalTicketsSold(){
    echo number_format(getTotalTicketsSold());
}

function getNodeCount(){
    $sql = "SELECT Count(ID) from nodes";
    return getSQLValue($sql);
}

function printNodeCount(){
    echo number_format(getNodeCount());
}

function getNodePaidAmount(){
    $sql = "select SUM(Amount)from node_payouts_history";
    return getSQLValue($sql);
}

function printNodePaidAmount(){
    echo number_format(getNodePaidAmount());
}

function getRunningNodesTable(){
        $sql = "SELECT ID, Node_Address, Node_Wallet, Total_Earnt FROM nodes";
    $rpc = getRPC();
    $res = get_db_value($sql);
    $string = "";
    while($row = mysqli_fetch_array($res))
    {
        $status = $rpc->fundamentalnodelist('status', $row[1]);
        if(empty($status)){
            $status = "Not found";
            $string=$string . "<tr><td>$row[0]</td><td>$row[1]</td><td>$row[2]</td><td>".number_format($row[3])." kB3</td><td>$status</td></tr>";
        }else{
            foreach($status as $key => $val){
                $string=$string. "<tr><td>$row[0]</td><td>$row[1]</td><td>$row[2]</td><td>".number_format($row[3])." kB3</td><td>$val</td></tr>";
            }
        }
    }
    return $string;
}

function getTotalNodesInNetwork(){
    $b3coin = getRPC();
    $res = $b3coin->fundamentalnode("count");
    return $res;
}

function printTotalNodesInNetwork(){
    echo number_format(getTotalNodesInNetwork());
}

function getNodeShareNetwork(){
    return round(getNodeCount()/getTotalNodesInNetwork()*100,4);
}

function printNodeShareNetwork(){
   echo getNodeShareNetwork() . '%';
}

function printNodeProgressBar(){
    $sql='select SUM(amount)+SUM(Direct_Deposit)+IFNULL((SELECT SUM(Amount) FROM node_deposit_pending),0)-(SELECT SUM(Cost) FROM nodes) from node_owners';
    $res =  getSQLValue($sql)-15000;
    $percentage = floor($res/15000*100);
    if($percentage > 100){
        $percentage =100;
    }
    echo "<div style='width: $percentage%'><div style='min-width: 200px'><center>".number_format($res)."/15,000 ($percentage%)</center></div></div>";
}

function getFirstGiveawayPrize(){
    return getGiveAwayAmount()*0.42;
}

function printFirstGiveawayPrize(){
    echo number_format(round(getFirstGiveawayPrize(),2),2);
}

function getSecondGiveawayPrize(){
    return getGiveAwayAmount()*0.15;
}

function printSecondGiveawayPrize(){
    echo number_format(round(getSecondGiveawayPrize(),2),2);
}

function getThirdGiveawayPrize(){
    return getGiveAwayAmount()*0.08;
}

function printThirdGiveawayPrize(){
    echo number_format(round(getThirdGiveawayPrize(),2),2);
}

function getFourthGiveawayPrize(){
    return getGiveAwayAmount()*0.01;
}

function printFourthGiveawayPrize(){
    echo number_format(round(getFourthGiveawayPrize(),2),2);
}

function printYesterday(){
    $date = new DateTime();
    $date->sub(new DateInterval('P1D'));
    echo $date->format('F j, Y') . "\n";
}

function getLastGiveawayPrizePool(){
    $sql = "SELECT Amount FROM daily_giveaway WHERE Paid=1 ORDER BY END DESC LIMIT 1";
    return getSQLValue($sql);
}

function printLastGiveawayPrizePool(){
    echo number_format(floor(getLastGiveawayPrizePool()));
}

function getCurrentGiveawayPrizePool(){
    $sql = "SELECT Amount FROM daily_giveaway WHERE START=UTC_DATE()";
    return getSQLValue($sql);
}

function printCurrentGiveawayPrizePool(){
    echo number_format(floor(getCurrentGiveawayPrizePool()));
}

function getLastGiveawayTickets(){
    $sql = "SELECT SUM(lottery_tickets)+SUM(referral_tickets) FROM giveaway_tickets WHERE giveaway_id = (SELECT MAX(ID) FROM daily_giveaway)-1";
    return getSQLValue($sql);
}

function printLastGiveawayTickets(){
    echo number_format(getLastGiveawayTickets());
}


function getLastGiveAway(){
    $sql = "SELECT Wallet_Address, Amount, Tickets FROM giveaway_payout_history ORDER BY giveaway_ID DESC, Amount DESC limit 10";
    $res = get_db_value($sql);
    $i=1;
    while($row = mysqli_fetch_array($res))
    {
        echo "<tr><td>". numberToPlace($i) . "</td><td>$row[0]</td><td>".number_format($row[2])."</td><td>" . number_format($row[1]) . " kB3</td></tr>";
        $i++;
    }
}

function getTimeTillUTC(){
    echo date("H:i:s", time() - date("Z"));
}

function getTotalBurned(){
    $sql = "SELECT SUM(Cost) FROM nodes";
    return getSQLValue($sql);
}

function printTotalBurned(){
    echo number_format(getTotalBurned());
}

function getGiveawayUserData($wallet){
    $sql = "SELECT lottery_tickets, referral_tickets FROM giveaway_tickets WHERE b3_wallet='$wallet' AND giveaway_id =".getGiveawayMAXID();
    $res = get_db_value($sql);
    $lottery=0;
    $referral=0;
    $total = getTotalGiveawayTickets();
    $string = "";
    while($row = mysqli_fetch_array($res))
    {
        $lottery = $row[0];
        $referral= $row[1];
    }
    if($total==0){
        $percentage =0;
    }else{
        $percentage = round(($lottery+$referral)/$total * 100,3);
    }
    if($referral > 0){
        $sql = "SELECT giveaway_tickets.b3_wallet, lottery_tickets FROM giveaway_tickets INNER JOIN referrals ON giveaway_tickets.b3_wallet=referrals.Wallet_Address WHERE giveaway_id=".getGiveawayMAXID()." and Link=(SELECT Link from wallet_addresses where b3_wallet='$wallet')";
        $res = get_db_value($sql);
        while($row = mysqli_fetch_array($res))
        {
              $string = $string . "<tr><td>$row[0]</td><td>" . number_format($row[1]) . "</td></tr>";
        }
    }
    return json_encode(array("total" => number_format($lottery+$referral), "lottery" => number_format($lottery), "referrals" => number_format($referral), "percentage" => $percentage, "table" => $string));
}

function getFaucetUserData($wallet){
  $sql = "SELECT sum(tickets_won), sum(b3_won), COUNT(*) FROM faucet_claims WHERE b3_wallet='$wallet'";
  $res = get_db_value($sql);
  while($row = mysqli_fetch_array($res))
  {
        return json_encode(array("tickets" => number_format($row[0]), "b3" => number_format($row[1],2), "count"=> number_format($row[2])));
  }
}

function printNodeRewardHistory(){
    echo getNodeRewardHistory();
}

function getNodeRewardHistory(){
    $table_rewards = "";
    $sql = "SELECT ID, Transaction, Amount, Date FROM node_rewards LEFT JOIN nodes ON Node_ID=Node_Address ORDER BY Date DESC limit 50";
    $res = get_db_value($sql);
    while($row = mysqli_fetch_array($res))
    {
        $table_rewards = $table_rewards. "<tr><td>$row[0]</td><td><a href='https://chainz.cryptoid.info/b3/tx.dws?$row[1]' target='_blank'>$row[1]</a></td><td>". number_format(round($row[2],5),5) ." kB3</td><td>".date('M j, Y', strtotime($row[3]))."</td></tr>";
    }
    return $table_rewards;
}

function getNodeUserData($wallet){
    $sql = "SELECT Amount, Node_Waiting_For, Deposit_Date FROM node_deposit_pending WHERE Wallet_Address='$wallet'";
    $res = get_db_value($sql);
    $table_pending = "";
    $pending = 0;
    while($row = mysqli_fetch_array($res))
    {
        $pending +=($row[0]*0.25);
        $table_pending = $table_pending. "<tr><td>".number_format($row[0])." kB3</td><td>$row[1]</td><td>".date('M j, Y', strtotime($row[2]))."</td></tr>";
    }
    $lottery=0;
    $deposit=0;
    $sql = "SELECT * FROM node_owners WHERE Wallet_Address='$wallet'";
    $res = get_db_value($sql);
    while($row = mysqli_fetch_array($res))
    {
        $lottery = $row[1];
        $deposit = $row[2];
    }
    $earned = 0;
    $count= 0;
    $sql = "SELECT COUNT(*), SUM(Amount) from node_payouts_history WHERE Wallet_Address='$wallet'";
    $res = get_db_value($sql);
    while($row = mysqli_fetch_array($res))
    {
        $count=$row[0];
        $earned=$row[1];
    }
    $total = getNodeKB3Amount();
    $percentage = ($lottery+$deposit+$pending)/$total * 100;
    return json_encode(array("pending_table" => $table_pending, "lottery" => number_format($lottery,5), "deposit" => number_format($deposit,5), "percentage" => number_format($percentage,5), "count" => number_format($count), "earned" => number_format($earned,5), "total" => number_format($lottery+$deposit,5)));
}

function getNodeUserDataTable($wallet){
    $table_rewards = "";
    $sql = "SELECT ID,node_rewards.Transaction as Node_Tx, node_payouts_history.Transaction as Pay_Tx, node_rewards.Amount, node_payouts_history.Amount, Date FROM node_rewards INNER JOIN nodes ON Node_Address=Node_ID RIGHT JOIN node_payouts_history ON node_rewards.Transaction = node_payouts_history.Paying_Tx WHERE Wallet_Address='$wallet' ORDER BY DATE DESC LIMIT 50";
    $res = get_db_value($sql);
    while($row = mysqli_fetch_array($res))
    {
        $table_rewards = $table_rewards. "<tr><td>$row[0]</td><td><a href='https://chainz.cryptoid.info/b3/tx.dws?$row[1]' target='_blank'>Reward Tx</a> | <a href='https://chainz.cryptoid.info/b3/tx.dws?$row[2]' target='_blank'>Payment Tx</a></td><td>". number_format(round($row[3],5),5) ." kB3</td><td>". number_format(round($row[4],5),5) ." kB3</td><td>".date('M j, Y', strtotime($row[5]))."</td></tr>";

    }
    return json_encode(array("table_rewards" => $table_rewards));
}

function insertCaptcha(){
    $rand = mt_rand(0,20);
    $element = null;
    if($rand != 20){
        $element =  "<center><script type='text/javascript'>
        var ACPuzzleOptions = {
            theme:	'white',
            lang:	'en',
            size:	'300x150'
        };
        </script>

        <script type='text/javascript'
        src='https://api-secure.solvemedia.com/papi/challenge.script?k=RshVkEz2EuN7uDFpG2z0n6u0mww7bK2E'>
        </script>

        <noscript>
           <iframe src='https://api-secure.solvemedia.com/papi/challenge.noscript?k=RshVkEz2EuN7uDFpG2z0n6u0mww7bK2E'
           height='300' width='500' frameborder='0'></iframe><br>
           <textarea name='adcopy_challenge' rows='3' cols='40'>
           </textarea>
           <input type='hidden' name='adcopy_response'
           value='manual_challenge'>
        </noscript></center>";
    }else{
        $element = '<center><div class="g-recaptcha" data-sitekey="6LfOXEwUAAAAACGwAIYA_qZAMbB5koZdEkMHvjdR"></div></center>';
    }
    echo $element;
}

function getTotalFaucetClaims(){
    $sql="SELECT sum(claim_count) FROM faucet_claims";
    return getSQLValue($sql);
}

function printTotalFaucetClaims(){
    echo number_format(getTotalFaucetClaims());
}

function getTotalFaucetClaimed(){
    $sql="SELECT sum(b3_won) + (sum(tickets_won)*10) FROM faucet_claims";
    return getSQLValue($sql);
}

function printTotalFaucetClaimed(){
    echo number_format(getTotalFaucetClaimed(),2);
}


?>
